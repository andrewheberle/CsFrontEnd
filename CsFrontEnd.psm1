#region Global variables

$ModuleName = "CsFrontEnd"

# List of configuration keys that can be changed
# These are used to validate input and also correspond to default aprameters

# Keys that are strings
$ValidConfigKeysString = @(
    "LogFile",
    "MaxLogSize",
    "FriendlyName",
    "Organization",
    "OU",
    "State",
    "City",
    "Country",
    "DomainName",
    "CA",
    "Template"
)

# Keys that are integers
$ValidConfigKeysInt = @()

# Keys that are arrays
$ValidConfigKeysArray = @("FrontEnds","CertificateType")

# Concatenation of the above arrays
$ValidConfigKeys = $ValidConfigKeysString + $ValidConfigKeysInt + $ValidConfigKeysArray

$ConfigLocations = @{
    Default = (Join-Path $PSScriptRoot "Default-$($ModuleName)Settings.ps1");
    Global = (Join-Path $env:ALLUSERSPROFILE "Global-$($ModuleName)Settings.ps1");
    User = (Join-Path $env:USERPROFILE "User-$($ModuleName)Settings.ps1")
}

#endregion

#region Unexported helper functions

function Is-ValidConfigKey {
    <#
    Helper function that checks that checks the provided key is valid
    #>
    Param(
        [parameter(Mandatory=$true)]
        [String]$Key
    )

    if ($Key -in $ValidConfigKeys) {
        $true
    } else {
        throw "The key ""$Key"" was not valid. Only the following keys can be set: $($ValidConfigKeys -join ", ")"
    }
}

function Write-Log {
    <#
    Helper function to write to log file
    #>
    [cmdletbinding(SupportsShouldProcess=$true,DefaultParameterSetName='Default')]
    Param (
        [parameter(Mandatory=$true,Position=0,ParameterSetName='Default')]
        [parameter(Mandatory=$true,Position=0,ParameterSetName='Quiet')]
        [parameter(Mandatory=$true,Position=0,ParameterSetName='Fatal')]
        [String]$Message,
        [parameter(ParameterSetName='Fatal')]
        [Switch]$Fatal,
        [parameter(ParameterSetName='Quiet')]
        [Switch]$Quiet,
        [parameter(Position=1)]
        [String]$LogFile = (Get-Default -Key LogFile)
    )

    # Add "WhatIf" tag for dry-runs
    if ($WhatIfPreference.IsPresent) {
        $Message = "(WhatIf) $Message"
    }

    # Ensure $LogFile path exists
    $LogFileDir = ([io.Fileinfo]$LogFile).DirectoryName
    if (-not (Test-Path -PathType Container $LogFileDir)) {
        try {
            # Create (including parent dirs with -Force)
            New-Item -Type Directory $LogFileDir -Force
        } catch {
            # If LogFileDir could not be created, write error and use default location
            $ErrorMessage = $_.Exception.Message
            $LogFile = (Get-Default -Key LogFile -Scope Default)
            Write-Error "Directory ""$LogFileDir"" for log file could not be created: $($ErrorMessage). Logging to: $LogFile"
        }
    }
    
    # Write to log with timestamp
    Write-Output "$(Get-Date -Format s) : $Message" | Out-File -WhatIf:$false -FilePath $LogFile -Append

    if ($Fatal) {
        # Write fatal message
        Throw "$Message"
    } else {
        if ($Quiet) {
            # When we are quiet only output to screen on "-Verbose"
            Write-Verbose "$Message"
        } else {
            # Write out to pipeline normally
            Write-Output "$Message"
        }
    }
}

function Expand-ZipFile {
    <#
    .SYNOPSIS

    Expands a zip file.
    #>
    [cmdletbinding(SupportsShouldProcess=$true)]
    Param(
        [String]$Path,
        [String]$DestinationPath
    )
    BEGIN { }
    PROCESS {
        if ($PSVersionTable.PSCompatibleVersions -contains "5.0") {
            Write-Verbose "Using PowerShell 5.0 native ""Expand-Archive"" command..."
            Expand-Archive -Path $Path -DestinationPath $DestinationPath -ErrorAction Stop
        } else {
            Write-Verbose "Using PowerShell 4.0 zip extraction method..."
            Add-Type -Assembly "System.IO.Compression.FileSystem" -ErrorAction Stop
            [IO.Compression.ZipFile]::ExtractToDirectory($Path, $DestinationPath)
        }
    }
    END { }

}

function Invoke-Robocopy {
    <#
    Helper function to invoke robocopy consistently to copy data from a local directory to an identical UNC path on remote machine(s)
    #>
    [cmdletbinding(SupportsShouldProcess=$true)]
    Param(
        [parameter(Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$Source,
        [parameter(Mandatory=$true)]
        [String]$DestinationHost,
        [String]$Filter,
        [String]$Options
    )

    BEGIN { }
    PROCESS {
        foreach ($s in $Source) {
            # Resolve path to ensure it is valid
            try {
                $ResolvedSource = [io.FileInfo](($s | Resolve-Path).Path)
            } catch {
                $ErrorMessage = $_.Exception.Message
                Write-Log -Fatal "Problem resolving path ""$s"": $ErrorMessage"
            }

            # Convert local path to UNC path on remote host
            $UncPath = "\\$($DestinationHost)\$(($ResolvedSource | Split-Path -Qualifier).Replace(':', '$'))$($ResolvedSource | Split-Path -NoQualifier)"
            if ($pscmdlet.ShouldProcess("$ResolvedSource -> $UncPath", "robocopy")) {
                # Run robocopy
                $Output = (& robocopy "$ResolvedSource" "$UncPath" "$Filter" $Options)
                # Write output if run with "-Verbose"
                Write-Verbose ($Output | Out-String)
            }
        }
    }
    END { }
}

#endregion

#region Exported functions

function Get-Default {
    <#
    .SYNOPSIS

    Gets a configuration key value

    .PARAMETER Key

    The Key to retrieve

    .PARAMETER Scope

    Specific scope to retrieve Key from. Without specifiying value the Key is retrieved in order of precedence from:

    3. Default config (distributed with Module)
    2. Global config (All Users profile)
    1. User config (Current Users profile)
    #>
    [cmdletbinding()]
    Param (
        [parameter(Mandatory=$true)]
        [ValidateScript({Is-ValidConfigKey $_})]
        [String]$Key,
        [ValidateSet("Default", "User", "Global")]
        [String]$Scope
    )

    BEGIN { }

    PROCESS {
        if ([string]::IsNullOrEmpty($Scope)) {
            # Check locations in order of precedence and dot-source file
            foreach ($Config in @($ConfigLocations.Default, $ConfigLocations.Global, $ConfigLocations.User)) {
                if (Test-Path -Path $Config -PathType Leaf) {
                    Write-Verbose "Loading defaults from ""$Config"""
                    . $Config
                }
            }
        } else {
            $Config = $ConfigLocations.Get_Item($Scope)
            if (Test-Path -Path $Config -PathType Leaf) {
                Write-Verbose "Loading defaults from ""$Config"""
                . $Config
            } else {
                Write-Verbose "Config for ""$Scope"" scope not found"
            }
        }

        # Return requested value
        Get-Variable -Name $Key -ValueOnly -ErrorAction SilentlyContinue
    }

    END { }
}

function Set-Default {
    <#
    .SYNOPSIS Sets configuration key values
    #>
    [cmdletbinding(SupportsShouldProcess=$true)]
    Param (
        [parameter(Mandatory=$true)]
        [ValidateScript({Is-ValidConfigKey $_})]
        [String]$Key,
        [parameter(Mandatory=$true,ParameterSetName='Set')]
        [String]$Value,
        [parameter(ParameterSetName='Set')]
        [Switch]$Add,
        [parameter(ParameterSetName='Default')]
        [Switch]$Default,
        [ValidateSet("User", "Global")]
        [String]$Scope = "User",
        [Switch]$NoSync
    )

    BEGIN { }
    
    PROCESS {
        # Get specified config
        $Config = $ConfigLocations.Get_Item($Scope)

        # Load the existing config (if any)
        if (Test-Path -Path $Config -PathType Leaf) {
            Write-Verbose "Current config:"
            Write-Verbose "$(Get-Content $Config)"
            # Dot source config file
            . $Config
        }

        # Check if adding to existing value was requested
        if ($Add) {
            # Adding to a setting only makes sense for arrays
            if ($Key -in $ValidConfigKeysArray) {
                # Get existing value if it exists
                $TempVal = Get-Variable -Name $Key -ValueOnly -ErrorAction SilentlyContinue
                # If it exists then add to it
                if ($TempVal -ne $null) {
                    $Value = ($TempVal -join ' ') + " $Value"
                }
            } else {
                Write-Error "Ignoring ""Add"" option for non-array..."
            }
        }

        if ($Default) {
            Write-Log "Setting config key ""$Key"" to default in ""$Scope"" scope..."
            try {
                Remove-Variable -Name $Key -ErrorAction Stop
            } catch {
                Write-Log "Config key ""$Key"" not set in ""$Scope"" scope."
            }
        } else {
            Write-Log "Setting config key ""$Key"" to ""$Value"" in ""$Scope"" scope..."
            # Set the variable to the new value
            Set-Variable -Name $Key -Value $Value
        }

        # Write config
        foreach ($Var in $ValidConfigKeys) {
            # Get variable/key
            $VarVal = Get-Variable -Name $Var -ValueOnly -ErrorAction SilentlyContinue
            # Check if it was set
            if ($VarVal -ne $null) {
                # Write values to temp file
                switch ($Var) {
                    { $_ -in $ValidConfigKeysString } {
                        $Line = "`$$Var = ""$VarVal"""
                    }
                    { $_ -in $ValidConfigKeysInt } {
                        $Line = "`$$Var = $VarVal"
                    }
                    { $_ -in $ValidConfigKeysArray } {
                        if ($VarVal -is [Array]) {
                            $Line = "`$$Var = `@`(""$($VarVal -join '", "')""`)"
                        } else {
                            $Line = "`$$Var = `@`(""$($VarVal -replace ' ', '", "')""`)"
                        }
                    }
                }
                Write-Log -Quiet "Writing ""$Line"" to ""$($Config).tmp"""
                # Write out to temporary file
                Write-Output $Line | Out-File -FilePath "$($Config).tmp" -Append
            }
        }

        # Move temp file into place
        Write-Log "Committing changes to config"
        try {
            Move-Item -Path "$($Config).tmp" -Destination $Config -Force
        } catch {
            $ErrorMessage = $_.Exception.Message
            Write-Log -Fatal "Problem moving new config into place : $ErrorMessage"
        }

        Write-Log "Config update complete."

        if (-not $NoSync) {
            # Unless told otherwise, sync to other Edges
            Sync-Config @PsBoundParameters
        }
    }

    END { }
}

function Update-Deployment {
    <#
    .SYNOPSIS

    Updates the config of a Front End.

    Options inlcude bootstrapping the FE and/or requesting and optionally assigning new certificates.

    .PARAMETER Bootstrap

    Sets if bootstrap will be run.

    .PARAMETER NewCertificate

    Controls wether a new certificate will be requested.

    .PARAMETER Assign

    Assigns the requested certificate to the FE.
    #>
    [cmdletbinding(SupportsShouldProcess=$true)]
    Param(
    	[Switch]$Bootstrap,
        [parameter(ParameterSetName='IssueCertificate',Mandatory=$true)]
    	[Switch]$NewCertificate,
        [parameter(ParameterSetName='IssueCertificate')]
        [Switch]$Assign,
        [parameter(ParameterSetName='IssueCertificate')]
        [String[]]$CertificateType = (Get-Default -Key CertificateType),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$FriendlyName = (Get-Default -Key FriendlyName),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$Organization = (Get-Default -Key Organization),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$OU = (Get-Default -Key OU),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$State = (Get-Default -Key State),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$City = (Get-Default -Key City),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$Country = (Get-Default -Key Country),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$DomainName = (Get-Default -Key DomainName),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$CA = (Get-Default -Key CA),
        [parameter(ParameterSetName='IssueCertificate')]
        [String]$Template = (Get-Default -Key Template)
    )

    BEGIN { }
    PROCESS {
        if ($Bootstrap) {
            Write-Verbose "Starting bootstrap..."
            if ($pscmdlet.ShouldProcess("$((Get-CsComputer -Local).Identity)", "Bootstrap-CsComputer")) {
                & "$($env:ProgramFiles)\Skype for Business Server 2015\Deployment\Bootstrapper.exe"
            }
            Write-Verbose "Bootstrap complete."
        }
        if ($NewCertificate) {
            if ([string]::IsNullOrEmpty($CA)) {
                Write-Log -Fatal "Error: An Enterprise CA must be specified."
            }
            Write-Verbose "Starting certificate request..."
            $CertificateRequest = @{
                Type = $CertificateType;
                ComputerFqdn = (Get-CsComputer -Local).Identity;
                FriendlyName = $FriendlyName;
                Organization = $Organization;
                OU = $OU;
                State = $State;
                City = $City;
                Country = $Country;
                PrivateKeyExportable = $true;
                AllSipDomain = $true;
                DomainName = $DomainName;
                CA = $CA;
                Template = $Template;
                ClientEKU = $true
            }
            try {
                Request-CsCertificate -New @CertificateRequest -ErrorAction Stop
            } catch {
                $ErrorMessage = $_.Exception.Message
                Throw "Problem with certificate request: $ErrorMessage"
            }
            Start-Sleep 5
            $Thumbprint = Get-ChildItem -Path cert:\LocalMachine\my | ? { $_.FriendlyName -eq $CertificateRequest.FriendlyName } | select -ExpandProperty Thumbprint
            if ($Assign) {
                Write-Verbose "Starting certificate assignment..."
                # Check we got a thumbprint back
                if ($Thumbprint -eq $null) {
                    Throw "Unable to retreive the thumbprint of the certificate named ""$($CertificateRequest.FriendlyName)"""
                }
                # Assign the certificate
                try {
                    Set-CsCertificate -Type $CertificateRequest.Type -Thumbprint $Thumbprint -ErrorAction Stop
                } catch {
                    $ErrorMessage = $_.Exception.Message
                    Throw "Problem with certificate assignment: $ErrorMessage"
                }
                Write-Verbose "Certificate assignment complete."
            }
            Write-Verbose "Certificate request complete."
        }
    }
    END { }
}

function Sync-Module {
    <#
    .SYNOPSIS

    Copies module to list of Front End servers

    .PARAMETER FrontEnds

    List (array) of Front End servers to transfer to.

    Defaults to value in config.
    #>
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param(
        [String[]]$FrontEnds = (Get-Default -Key FrontEnds),
        [parameter(ValueFromRemainingArguments=$true)]
        $Leftovers
    )

    BEGIN { }

    PROCESS {
        Write-Log "Starting module sync process..."
        # Copy module to other Edge servers pool
        foreach ($Fe in $FrontEnds) {
            if ($env:COMPUTERNAME -eq $Fe) {
                Write-Log "Skipping copy to self."
                continue
            }
            Write-Log "Transferring module to ""$Fe""..."
            Invoke-Robocopy -Source $PSScriptRoot -DestinationHost $Fe -Options "/MIR"
        }
        Write-Log "Module sync finished."
    }

    END { }
}

function Sync-Config {
    <#
    .SYNOPSIS

    Copies configuration to list of Front End servers

    .PARAMETER FrontEnds

    List (array) of Front End servers to transfer to.

    Defaults to value in config.
    #>
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param(
        [String[]]$FrontEnds = (Get-Default -Key FrontEnds),
        [parameter(ValueFromRemainingArguments=$true)]
        $Leftovers
    )

    BEGIN { }

    PROCESS {
        Write-Log "Starting config sync process..."
        # Copy module to other Edge servers pool
        foreach ($Fe in $FrontEnds) {
            if ($env:COMPUTERNAME -eq $Fe) {
                Write-Log "Skipping copy to self."
                continue
            }

            # Copy config(s) (if any)
            foreach ($Config in @($ConfigLocations.Global, $ConfigLocations.User)) {
                if (Test-Path -Path $Config -PathType Leaf) {
                    Write-Log "Transferring config to ""$Fe""..."
                    Invoke-Robocopy -Source ([io.Fileinfo]$Config | Split-Path -Parent) -DestinationHost $Fe -Filter ([io.Fileinfo]$Config | Split-Path -Leaf)
                }
            }
        }
        Write-Log "Config sync finished."
    }

    END { }
}

function Update-Module {
    <#
    .SYNOPSIS

    Downloads the specified version of the module from repository and installs into the specified scope (User or Global)

    .PARAMETER Version

    Version to download. Mandatory.

    .PARAMETER Scope

    Specifies if the module is downloaded to the current users modules direcotry or the global/system location.

    Defaults to "User"

    .PARAMETER Force

    Allows overwriting of existing module directory
    #>
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param(
        [parameter(Mandatory=$true)]
        [String]$Version,
        [ValidateSet("User", "Global")]
        [String]$Scope = "User",
        [Switch]$Force
    )

    BEGIN { }

    PROCESS {
        switch ($Scope) {
            "User" {
                $ModulePath = "$($Home)\Documents\WindowsPowerShell\Modules"
            }
            "Global" {
                $ModulePath = "$($PSHome)\Modules"
            }
        }

        $Uri = "https://gitlab.com/andrewheberle/$($ModuleName)/repository/$($Version)/archive.zip"
        $DownloadFile = "$($env:TEMP)\$($ModuleName)-$($Version).zip"
        $TempExtractPath = "$($env:TEMP)\$($ModuleName)-$($Version)"
        if ($PSVersionTable.PSCompatibleVersions -contains "5.0") {
            # For Powershell 5 save into version specific sub-directory
            $TargetPath = "$($ModulePath)\$($ModuleName)\$($Version)"
        } else {
            # For Powershell 4 save direct to module dir
            $TargetPath = "$($ModulePath)\$($ModuleName)"
        }

        if ((Test-Path -Path $TargetPath -Type Container) -and (-not $Force)) {
            Write-Log -Fatal "$TargetPath exists. Not overwriting without ""Force"" option specified."
        }

        Write-Log "Downloading version ""$Version"" of module..."
        try {
            Invoke-WebRequest -Uri $Uri -OutFile $DownloadFile -ErrorAction Stop
        } catch {
            if ($_.Exception.Response.StatusCode -eq "NotFound") {
                Write-Log "Module version ""$Version"" was not found."
                return
            } else {
                $ErrorMessage = $_.Exception.Message
                $LineNo = $_.Exception.Line
                Write-Log -Fatal "Problem downloading module : $ErrorMessage At line:$($LineNo)"
            }
        }

        Write-Log "Extracting downloaded module to ""$TempExtractPath""..."
        try {
            Expand-ZipFile -Path $DownloadFile -DestinationPath $TempExtractPath -ErrorAction Stop
        } catch {
            $ErrorMessage = $_.Exception.Message
            $LineNo = $_.Exception.Line
            Write-Log -Fatal "Problem extracting module : $ErrorMessage At line:$($LineNo)"
        }

        Write-Log "Creating target path if required..."
        try {
            New-Item -Path $TargetPath -Type Directory -Force -ErrorAction Stop | Out-Null
        } catch {
            $ErrorMessage = $_.Exception.Message
            $LineNo = $_.Exception.Line
            Write-Log -Fatal "Problem creating target path : $ErrorMessage At line:$($LineNo)"
        }

        Write-Log "Moving into place..."
        try {
            if ($PSVersionTable.PSCompatibleVersions -contains "5.0") {
                foreach ($file in (Get-ChildItem "$($TempExtractPath)\$((Get-ChildItem $TempExtractPath -Depth 0 | Select -ExpandProperty Name))")) {
                    Write-Verbose "Copying ""$file"" to ""$TargetPath""..."
                    Move-Item -Path $file.FullName -Destination $TargetPath -Force -ErrorAction Stop
                }
            } else {
                foreach ($file in (Get-ChildItem "$($TempExtractPath)\$((Get-ChildItem $TempExtractPath -Recurse | % { if (($_.FullName.ToString() | Select-String "\\" -AllMatches).Matches.Count -le (($TempExtractPath | Select-String "\\" -AllMatches).Matches.Count + 1)) { $_ } } | Select -ExpandProperty Name))")) {
                    Write-Verbose "Copying ""$file"" to ""$TargetPath""..."
                    Move-Item -Path $file.FullName -Destination $TargetPath -Force -ErrorAction Stop
                }
            }
        } catch {
            $ErrorMessage = $_.Exception.Message
            $LineNo = $_.Exception.Line
            Write-Log -Fatal "Problem moving into place : $ErrorMessage At line:$($LineNo)"
        }

        Write-Log "Removing temporary files..."
        try {
            Remove-Item -Path $TempExtractPath -Recurse -Force -Confirm:$false -ErrorAction Stop
            Remove-Item -Path $DownloadFile -Force -Confirm:$false -ErrorAction Stop
        } catch {
            $ErrorMessage = $_.Exception.Message
            $LineNo = $_.Exception.Line
            Write-Log -Fatal "Problem removing temporary files : $ErrorMessage At line:$($LineNo)"
        }
    }

    END { }
}

#endregion

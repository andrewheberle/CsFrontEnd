# Log file
$LogFile = (Join-Path $env:TEMP "$($ModuleName).log")
# Certificate type to set
$CertificateType = @("Default","WebServicesInternal","WebServicesExternal")
$MaxLogSize = "512kb"
# List of Front Ends
$FrontEnds = @()
# Certificate details
$FriendlyName = "Skype for Business Server 2015 Default certificate $(Get-Date -f "dd/MM/yyyy")";
$Organization = ""
$OU = "IT"
$State = ""
$City = ""
$Country = "AU"
$DomainName = "$((Get-CsComputer -Local).Pool),sip.$((Get-CsSipDomain | Select -ExpandProperty Name) -join ",sip.")"
$CA = ""
$Template = "WebServer"
